<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p>Startar aplicação:</p>
<p>Baixar Laravel 5.;</p>
<p>Clonar esse projeto;</p>
<p>Acessar a pasta via terminal e usar o comando php artisan serve.</p>

<P>A classe App\Constants\Constants detém das informações que definem a quantidade de Heróis e histórias que são listadas, assim como urls importantes e está registrada como alias no Arquivo de configurações app.php</p>
