<?php

namespace App\Http\Controllers;

use App\classes\Marvel;
use App\ListaBusiness;

class ListaController extends Controller
{

    public function index(){

        $marvel = new Marvel();
        $heroLista = $marvel->heroList();

        $business = new ListaBusiness();
        $lista = $business->lista($heroLista,$marvel);

        return view('welcome',['lista'=> $lista]);

    }
}