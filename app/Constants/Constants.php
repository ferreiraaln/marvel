<?php

namespace App\Constants;

class Constants{

    const HEROLIMIT = 3;
    const STORIESLIMIT = 5;
    const HEROURL = "characters";
    const STORIESURL = "stories";
    const MARVELURL = "http://gateway.marvel.com/v1/public/";
    const PUBLIC_KEY = "dea80b2f2efb948246b6df74c657adea";
    const PRIVATE_KEY = "45e4d6f1ccb6639bb09b1658991ebb591bd9bab2"; 

}