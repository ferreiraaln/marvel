<?php

namespace App;

use App\classes\Hero;
use App\classes\Stories;
use App\classes\thumbnail;

class ListaBusiness{

    public function lista($heroLista,$marvel){
        return $this->listaHero($heroLista,$marvel);
    }

    public function listaHero($heroLista,$marvel){

        $lista = array();

        foreach ($heroLista['data']['results'] as $heros => $h) {

            $hero  = new Hero();
            $hero->setId($h['id']);
            $hero->setName($h['name']);
            $hero->setDescrption($h['description']);


            $tumb  = new thumbnail($h['thumbnail']['path'],$h['thumbnail']['extension']);
            $hero->setThumbnail($tumb->image());

            $storiesList = $marvel->storiesList($hero->getId());

            $hero->setStories( $this->listaStories($storiesList) );

            $lista[] = $hero;
        }

        return $lista;

    }

    public function listaStories($storiesList){

        $stores = array();

        foreach ($storiesList['data']['results'] as $stories => $s) {
           $storie  = new Stories();
           $storie->setId($s['id']);
           $storie->setTitle($s['title']);
           $storie->setResource($s['resourceURI']);

           $stores[] = $storie;
        }

        return $stores;
    }
  
}