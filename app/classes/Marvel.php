<?php

namespace App\classes;

use Carbon\Carbon;
use App\classes\Curl;
use App\Constants\Constants;

class Marvel{

    private $privateKey = Constants::PRIVATE_KEY;
    private $publicKey = Constants::PUBLIC_KEY;
    private $url = Constants::MARVELURL;
    private $hash;
    private $limit;
    private $timestamp;

    public function __construct(){
    	$this->setHash();
    	$this->setTimestamp(Carbon::now()->timestamp); 
    }

    public function getPrivateKey(){
    	return $this->privateKey;
    }

    public function getPublicKey(){
    	return $this->publicKey;
    }

    public function getLimit(){
    	return $this->limit;
    }

    public function setLimit($limit){

      if( !(($limit > 0  ) && ($limit <= 100) && is_numeric($limit)) ){
        $limit = 3;
      }

    	$this->limit = $limit;
    }

    public function getTimestamp(){
    	return $this->timestamp;
    }

    public function setTimestamp($timestamp){
    	$this->timestamp = $timestamp;
    }

    public function getHash(){
    	return $this->hash;
    }

    public function getUrl(){
    	return $this->url;
    }

    public function setHash(){

    	$timestamp = Carbon::now()->timestamp;
    	$this->hash = md5($timestamp 
    		. $this->getPrivateKey() 
    		. $this->getPublicKey()
		  );
    }

    public function heroList(){
      $url = $this->getUrl();
      $url .= Constants::HEROURL.'?';
      $this->setLimit(Constants::HEROLIMIT);
      return $this->curl($url);
    }

    public function storiesList($id){
      $url = $this->getUrl();
      $url .= Constants::HEROURL."/".$id."/". Constants::STORIESURL ."?";
      $this->setLimit(Constants::STORIESLIMIT);
      return $this->curl($url);
    }

    public function curl($url){

    	$url .=	'ts='.$this->getTimestamp()
    			.'&apikey='.$this->getPublicKey() 
    			.'&hash='.$this->getHash() 
    			.'&limit='.$this->getLimit();

		$curl = new Curl();
		return $curl->get($url); 
    }
    
}