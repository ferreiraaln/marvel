<?php

namespace App\classes;
use App\classes\Stories;

class Hero{
    
    private $id;
    private $name;
    private $description;
    private $thumbnail;
    private $stories;

    public function __construct(){

    }

    public function setDescrption($description){
        $this->description = $description;
    }

    public function getDescrption(){
        return $this->description;
    }

    public function setStories($stories){
        $this->stories = $stories;
    }

    public function getStories(){
        return $this->stories;
    }

    public function setThumbnail($thumbnail){
        $this->thumbnail = $thumbnail;
    }

    public function getThumbnail(){
        return $this->thumbnail;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getId(){
        return $this->id;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getName(){
        return $this->name;
    }

    
}