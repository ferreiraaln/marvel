<?php

namespace App\classes;


class Stories{
    
    private $id;
    private $title;
    private $resource;

    public function __construct(){

    }

    public function setResource($resource){
        $this->resource = $resource;
    }

    public function getResource(){
        return $this->resource;
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getId(){
        return $this->id;
    }
    
}