<?php

namespace App\classes;


class Thumbnail{
    
    private $path;
    private $extension;

    public function __construct($path,$extension){
        $this->path = $path;
        $this->extension = $extension;
    }

    public function image(){
        return $this->path .".". $this->extension;
    } 
}